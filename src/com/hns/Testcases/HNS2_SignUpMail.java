package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

public class HNS2_SignUpMail {

	WebDriver driver = null;
	WebElement element = null;
	String testCaseName = "HNS2_SignUpMail";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	Map<String, String> data = new HashMap<String, String>();
	User user = new User();
	int stepno = 0;

	@BeforeClass
	public void before() throws Exception {
		System.out.println(testCaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void signUpusingMail() throws Exception {
		try {
			TestLog.init(testCaseName, "Verify user should be able to signup using Email option");
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();	
			TestLog.step(++stepno, "Enter Details and Sign Up");
			user.signUpUsingMail(driver, data);
			TestLog.step(++stepno, "Verify Details after Sign Up");
			user.signUpVerifyDetails(driver, data);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@AfterClass
	public void after() throws Exception {
		System.out.println(testCaseName + ": Shutting down execution...");
		driver.quit();
	}
}

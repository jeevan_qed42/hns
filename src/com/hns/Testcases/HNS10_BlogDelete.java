package com.hns.Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hns.Genric.Admin;
import com.hns.Genric.Blog;
import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.DateConverter;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS10_BlogDelete {

	WebDriver driver = null;
	WebElement element = null;
	String screenShotName = null;
	String testcaseName = "HNS10_BlogDelete";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Blog blog = new Blog();
	Date systemDate = new Date();
	Map<String, String> data = new HashMap<String, String>();
	DateConverter dateConverter = new DateConverter();
	WebDriverWait wait;
	Admin admin = new Admin();
	int stepno = 0;

	@BeforeMethod
	public void beforeClass() throws Exception {
		System.out.println(testcaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void deleteBlog() throws Exception {
		try {
			TestLog.init(testcaseName, "Verify stylish should be able to delete the blog");
			// Load Test Data
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testcaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			TestLog.init(testcaseName, "Verify Stylish should be able to delete the Blog");

			TestLog.step(++stepno, "Login with the Stylish Credentials");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			TestLog.step(++stepno, "Visit the blog Tab");
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
			String blogName = data.get("BlogTitle") + "_" + dateFormat.format(systemDate);
			data.replace("BlogTitle", blogName);
			TestLog.step(++stepno, "Create a Blog");
			blog.createBlog(driver, data);

			Thread.sleep(5000);
			TestLog.step(++stepno, "Logout from stylish account");
			user.logout(driver);

			TestLog.step(++stepno, "Login as Admin");
			admin.login(driver, data.get("AdminId"), data.get("AdminPassword"));
			TestLog.step(++stepno, "Search for the Blog");
			admin.searchContent(driver, data.get("BlogName"));
			TestLog.step(++stepno, "Update the status of Blog to declined");
			admin.updateBlogStatus(driver, "Decline");
			TestLog.step(++stepno, "Logout from Admin tab");
			admin.logout(driver);

			TestLog.step(++stepno, "Login as a Stylish");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			TestLog.step(++stepno, "Visit Blog page");
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			TestLog.step(++stepno, "Verify Blog should not appear in Pending tab");
			WebElement blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNull("Declined blog appears in 'Pending' tab", blogPending);

			TestLog.step(++stepno, "Verify Blog should not appear in Live tab");
			WebElement blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNull("Declined blog appears in 'Live' tab", blogLive);

			TestLog.step(++stepno, "Verify Blog should appear in Declined tab");
			WebElement blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Declined blog does not appear in 'Declined' tab", blogDeclined);

			// Edit and delete the blog
			TestLog.step(++stepno, "Click on Edit Blog");
			element = blogDeclined.findElement(By.xpath(Property.get("Profile.declineBlogEdit")));
			element.click();
			element = driver.findElement(By.className(Property.get("Profile.createPostFrame")));
			driver.switchTo().frame(element);
			TestLog.step(++stepno, "Delete the Blog");
			blog.deleteBlog();

			TestLog.step(++stepno, "Search blog in Live tab");
			blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNull("Deleted declined blog appears in 'Live' tab", blogLive);
			TestLog.step(++stepno, "Search blog in Declined tab");
			blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNull("Deleted declined blog appears in 'Declined' tab", blogDeclined);
			TestLog.step(++stepno, "Search blog in Pending tab");
			blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNull("Deleted appears in 'Pending' tab", blogPending);
			TestLog.pass();

		} catch (Exception | AssertionError error) {
			System.out.println("Testcase Failed...");
			if (driver != null) {
				screenshotName = Screenshot.capture(driver, testcaseName);
			}
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@AfterMethod
	public void afterClass() {
		System.out.println(testcaseName + ": Shutting down execution...");
		driver.quit();
	}

}

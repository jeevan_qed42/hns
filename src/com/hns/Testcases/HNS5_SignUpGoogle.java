package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.Property;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS5_SignUpGoogle {

	WebDriver driver = null;
	WebElement element = null;
	String testCaseName = "HNS5_SignUpGoogle";
	String screenShotName = null;
	ReadExcel excel = new ReadExcel();
	Map<String, String> data = new HashMap<String, String>();
	User user = new User();
	int stepno = 0;

	@BeforeMethod
	public void before() throws Exception {
		System.out.println(testCaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void SignUpUsingGoogle() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 1", "Verify user should be able to sign up using Google");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();
			TestLog.step(++stepno, "Enter User details and Sign up using Google");
			user.signUpUsingGoogle(driver, data);
			TestLog.step(++stepno, "Verify User details after Sign up using Google");
			user.signUpVerifyDetails(driver, data);
			TestLog.pass();

		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenShotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenShotName);
			throw error;
		}
	}

	@Test(dependsOnMethods = { "SignUpUsingGoogle" })
	public void signUpUsingExistingGoogleId() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 2", "Verify user should  get logged in, when user sign up using existing google ID");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();
			TestLog.step(++stepno, "Click on SignUp using Google button");
			element = driver.findElement(By.xpath(Property.get("Register.signUpGoogle")));
			element.click();
			
			element = driver.findElement(By.xpath(Property.get("Register.gmailUsername")));
			element.clear();
			element.sendKeys(data.get("GoogleUsername"));

			//element = driver.findElement(By.xpath(Property.get("Register.gmailNext")));
			//element.click();

			WebDriverWait wait = new WebDriverWait(driver, 5); 
			element = driver.findElement(By.xpath(Property.get("Register.gmailPassword")));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.clear();
			element.sendKeys(data.get("GooglePassword"));

			element = driver.findElement(By.xpath(Property.get("Register.gmailSignIn")));
			element.click();

			if (driver.getCurrentUrl().contains("https://accounts.google.com/o/oauth2/auth?")) {
				wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.gmailAllow")))));

				driver.findElement(By.xpath(Property.get("Register.gmailAllow"))).click();
			}

			
			TestLog.step(++stepno, "Verify user get logged in");
			element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			TestLog.step(++stepno, "Logout from account");
			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenShotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenShotName);
			throw error;
		}
	}
	
	@Test(dependsOnMethods = { "SignUpUsingGoogle" })
	public void signInUsingGoogle() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 3", "Verify user should be able to sign in using Google");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();
			TestLog.step(++stepno, "Click on Login button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeLogin")));
			element.click();
			TestLog.step(++stepno, "Click on Sign In using Google button");
			element = driver.findElement(By.xpath(Property.get("LoginPage.signInUsingGoogle")));
			element.click();
			
			TestLog.step(++stepno, "Enter Google details");
			
			element = driver.findElement(By.xpath(Property.get("Register.gmailUsername")));
			element.clear();
			element.sendKeys(data.get("GoogleUsername"));

			//element = driver.findElement(By.xpath(Property.get("Register.gmailNext")));
			//element.click();

			WebDriverWait wait = new WebDriverWait(driver, 5); 
			element = driver.findElement(By.xpath(Property.get("Register.gmailPassword")));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.clear();
			element.sendKeys(data.get("GooglePassword"));

			element = driver.findElement(By.xpath(Property.get("Register.gmailSignIn")));
			element.click();

			if (driver.getCurrentUrl().contains("https://accounts.google.com/o/oauth2/auth?")) {
				wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.gmailAllow")))));

				driver.findElement(By.xpath(Property.get("Register.gmailAllow"))).click();
			}

			
			TestLog.step(++stepno, "Verify user should be logged in");
			element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
			Assert.assertTrue("User failed to login when trying to login with Google ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
			Assert.assertTrue("User failed to login when trying to login with Google ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
			Assert.assertTrue("User failed to login when trying to login with Google ID", element.isDisplayed());

			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenShotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenShotName);
			throw error;
		}
	}

	@AfterMethod
	public void after() throws Exception {
		System.out.println(testCaseName + ": Shutting down execution...");
		driver.quit();
	}
}

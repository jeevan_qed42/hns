package com.hns.Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Blog;
import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.DateConverter;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS7_CreateBlog {

	WebDriver driver = null;
	WebElement element = null;
	String screenShotName = null;
	String testCaseName = "HNS7_CreateBlog";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Blog blog = new Blog();
	Date systemDate = new Date();
	Map<String, String> data = new HashMap<String, String>();
	DateConverter dateConverter = new DateConverter();
	int stepno = 0;

	@BeforeClass
	public void before() throws Exception {
		System.out.println(testCaseName + "Starting execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void CreateBlog() throws Exception {

		try {
			TestLog.init(testCaseName, "Verify stylish should be able to create blog");
			// Get test data
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			// Login with Stylish and visit Blog tab
			TestLog.step(++stepno, "Login using Stylish");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Add date to blog name and create blog
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
			String blogName = data.get("BlogTitle") + "_" + dateFormat.format(systemDate);
			data.replace("BlogTitle", blogName);
			blog.createBlog(driver, data);

			Thread.sleep(2000);
			
			// Search blog in Declined tab
			TestLog.step(++stepno, "Search blog in Declined tab");
			WebElement blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNull("Newly created blog appears in 'Declined' tab", blogDeclined);
			// Search blog in Live tab
			TestLog.step(++stepno, "Search blog in Live tab");
			WebElement blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNull("Newly created blog appears in 'Live' tab", blogLive);
			// Search blog in Pending tab
			TestLog.step(++stepno, "Search blog in Pending tab");
			WebElement blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Newly created blog does not appear in 'Pending' tab", blogPending);

			// Verify blog Date in Pending tab
			TestLog.step(++stepno, "Verify blog Date in Pending tab");
			element = blogPending.findElement(By.xpath(Property.get("Profile.pendingBlogDate")));
			String actualDate = element.getText();
			String expectedDate = dateConverter.getCurrentDateInBlogFormat();
			Assert.assertEquals("Date of blog in pending is not as expected", expectedDate, actualDate);

			// Verify blog details by accessing the blog
			TestLog.step(++stepno, "Verify blog details by accessing the blog");
			element = blogPending.findElement(By.xpath(Property.get("Profile.pendingBlogTitle")));
			element.click();
			blog.verifyBlogDetails(driver, data);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenShotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Testcase Failed...");
			TestLog.fail(screenShotName);
			throw error;
		}
	}

	@AfterClass
	public void after() throws Exception {
		System.out.println(testCaseName + "Shutting down the execution...");
		driver.quit();
	}
}

package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.Email;
import com.hns.Utilities.Property;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS6_ForgotPassword {

	WebDriver driver = null;
	WebElement element = null;
	Screenshot screen = new Screenshot();
	String screenShotName = null;
	String testCaseName = "HNS6_ForgotPassword";
	Email email = new Email();
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Map<String, String> data = new HashMap<String, String>();
	int stepno = 0;

	@BeforeMethod
	public void before() throws Exception {
		System.out.println(testCaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void forgotPassword() throws Exception {

		try {
			TestLog.init(testCaseName, "Verify user should be able to reset the password");
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			TestLog.step(++stepno, "Click on Login button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeLogin")));
			element.click();
			TestLog.step(++stepno, "Click on forgot password link");
			element = driver.findElement(By.xpath(Property.get("LoginPage.forgotPasswordLink")));
			element.click();

			TestLog.step(++stepno, "Enter Email Id");
			element = driver.findElement(By.xpath(Property.get("LoginPage.forgotPasswordEmailID")));
			element.clear();
			element.sendKeys(data.get("EmailId"));
			TestLog.step(++stepno, "Click on Submit button");
			element = driver.findElement(By.xpath(Property.get("LoginPage.forgotPasswordSubmit")));
			element.click();

			TestLog.step(++stepno, "Verify status message after submission");
			element = driver.findElement(By.xpath(Property.get("LoginPage.statusMessage")));
			String statusMessage = element.getText();
			Assert.assertEquals("Wrong message shown when user send email for forgot password",
					"Further instructions have been sent to your e-mail address.", statusMessage);

			TestLog.step(++stepno, "Fetch Password reset link from email and vist the link");
			String resetPasswordLink = email.getPasswordResetLink(data.get("EmailId"), data.get("EmailPassword"));
			driver.get(resetPasswordLink);
			element = driver.findElement(By.xpath(Property.get("LoginPage.forgotPasswordSubmit")));
			element.click();

			TestLog.step(++stepno, "Verify status Message after login");
			element = driver.findElement(By.xpath(Property.get("Profile.messageStatus")));
			String statusMessageAfterlogin = element.getText();
			Assert.assertEquals("Wrong message shown when user login after password reset",
					"You have just used your one-time login link. It is no longer necessary to use this link to log in. Please change your password.",
					statusMessageAfterlogin);

			TestLog.step(++stepno, "Enter new password and confirm password");
			element = driver.findElement(By.xpath(Property.get("Profile.password")));
			element.sendKeys(data.get("NewPassword"));

			element = driver.findElement(By.xpath(Property.get("Profile.confirmPassword")));
			element.sendKeys(data.get("ConfirmPassword"));

			TestLog.step(++stepno, "Click on Save Changes");
			element = driver.findElement(By.xpath(Property.get("Profile.saveChanges")));
			element.click();

			TestLog.step(++stepno, "Verify status message");
			element = driver.findElement(By.xpath(Property.get("Profile.messageStatus")));
			String statusMessageAfterReset = element.getText();
			Assert.assertEquals("Wrong message shown when user change the password", "The changes have been saved.",
					statusMessageAfterReset);

			TestLog.step(++stepno, "Logout from the account");
			user.logout(driver);
			TestLog.step(++stepno, "Login using new password");
			user.login(driver, data.get("EmailId"), data.get("NewPassword"));
		} catch (Exception | AssertionError error) {
			System.out.println("Testcase Failed...");
			if (driver != null) {
				screenShotName = Screenshot.capture(driver, testCaseName);
			}
			System.out.println("Failed execution...");
			TestLog.fail(screenShotName);
			throw error;
		}

	}

	@AfterMethod
	public void afterClass() {
		System.out.println(testCaseName + ": Shutting down execution...");
		driver.quit();
	}

}

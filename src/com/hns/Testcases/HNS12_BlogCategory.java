package com.hns.Testcases;

import org.testng.annotations.Test;

import com.hns.Genric.Admin;
import com.hns.Genric.Blog;
import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.DateConverter;
import com.hns.Utilities.Property;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

import org.testng.annotations.BeforeMethod;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;

public class HNS12_BlogCategory {
	WebDriver driver = null;
	WebElement element = null;
	String screenShotName = null;
	String testCaseName = "HNS12_BlogCategory";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Blog blog = new Blog();
	Date systemDate = new Date();
	Map<String, String> data = new HashMap<String, String>();
	DateConverter dateConverter = new DateConverter();
	WebDriverWait wait;
	Admin admin = new Admin();
	int stepno = 0;

	@BeforeMethod
	public void beforeMethod() throws Exception {
		System.out.println(testCaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void blogCategory() throws Exception {
		try {
			TestLog.init(testCaseName, "Verify newly created blog should appear in appropiate category");
			// Load Test Data
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			TestLog.init(testCaseName, "Verify Blogs are disaplyed as per category");

			TestLog.step(++stepno, "Login with the Stylish Credentials");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			TestLog.step(++stepno, "Visit the blog Tab");
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
			String blogName = data.get("BlogTitle") + "_" + dateFormat.format(systemDate);
			data.replace("BlogTitle", blogName);
			TestLog.step(++stepno, "Create a Blog");
			blog.createBlog(driver, data);
			Thread.sleep(5000);
			TestLog.step(++stepno, "Logout from Stylish account");
			user.logout(driver);
			
			TestLog.step(++stepno, "Login with Admin credentials");
			admin.login(driver, data.get("AdminId"), data.get("AdminPassword"));
			TestLog.step(++stepno, "Search Blog in Contents");
			admin.searchContent(driver, data.get("BlogTitle"));
			TestLog.step(++stepno, "Edit and update the Blog status to Approved");
			admin.updateBlogStatus(driver, "Approve");
			TestLog.step(++stepno, "Logout from Admin account");
			admin.logout(driver);
			
			TestLog.step(++stepno, "Go to Parlour page as anonymous user");
			element = driver.findElement(By.xpath(Property.get("HomePage.theParlour")));
			element.click();
			WebDriverWait wait = new WebDriverWait(driver, 10);
			TestLog.step(++stepno, "Visit Didactic Tab and Verify if Blog appears");
			element = driver.findElement(By.xpath(Property.get("ParlourPage.menuDidactic")));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			
			WebElement didactic = blog.searchBlog(driver, data.get("BlogTitle"));
			if (data.get("Category").equalsIgnoreCase("Didactic")) {
				Assert.assertNotNull("Blog not found in Didactic tab", didactic);
				blog.verifyBlogTeaserInParlour(didactic, Property.get("GlobalConfig.didacticColour"), data.get("StylishUserId"));

			} else {
				Assert.assertNull("Non Didactic blog found in didactic tab", didactic);
			}

			TestLog.step(++stepno, "Visit Lifestyle Tab and Verify if Blog appears");
			element = driver.findElement(By.xpath(Property.get("ParlourPage.menuLifestyle")));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			WebElement lifestyle = blog.searchBlog(driver, data.get("BlogTitle"));
			if (data.get("Category").equalsIgnoreCase("Lifestyle")) {
				Assert.assertNotNull("Blog not found in Lifestyle tab", lifestyle);
				blog.verifyBlogTeaserInParlour(lifestyle, Property.get("GlobalConfig.lifestyleColour"), data.get("StylishUserId"));
			} else {
				Assert.assertNull("Non Lifestyle blog found in Lifestyle tab", lifestyle);
			}

			TestLog.step(++stepno, "Visit Wardrobe Tab and Verify if Blog appears");
			element = driver.findElement(By.xpath(Property.get("ParlourPage.menuWardrobe")));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			
			WebElement wardrobe = blog.searchBlog(driver, data.get("BlogTitle"));
			if (data.get("Category").equalsIgnoreCase("Wardrobe")) {
				Assert.assertNotNull("Blog not found in Wardrobe tab", wardrobe);
				blog.verifyBlogTeaserInParlour(wardrobe, Property.get("GlobalConfig.wardrobeColour"), data.get("StylishUserId"));
			} else {
				Assert.assertNull("Non Wardrobe blog found in Wardrobe tab", wardrobe);
			}

			TestLog.step(++stepno, "Visit Spotlight Tab and Verify if Blog appears");
			element = driver.findElement(By.xpath(Property.get("ParlourPage.menuSpotlight")));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
			
			WebElement spotlight = blog.searchBlog(driver, data.get("BlogTitle"));
			if (data.get("Category").equalsIgnoreCase("Spotlight")) {
				Assert.assertNotNull("Blog not found in Spotlight tab", spotlight);
				blog.verifyBlogTeaserInParlour(wardrobe, Property.get("GlobalConfig.spotlightColour"), data.get("StylishUserId"));
			} else {
				Assert.assertNull("Non Spotlight blog found in Spotlight tab", spotlight);
			}

			TestLog.pass();

		} catch (Exception | AssertionError error) {
			System.out.println("Testcase Failed...");
			if (driver != null) {
				screenshotName = Screenshot.capture(driver, testCaseName);
			}
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}

	}

	@AfterMethod
	public void afterMethod() {
		System.out.println(testCaseName + ": Shutting down execution...");
		driver.quit();
	}

}

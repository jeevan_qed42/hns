package com.hns.Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Admin;
import com.hns.Genric.Blog;
import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.DateConverter;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;
import com.hns.Utilities.UploadFile;

import junit.framework.Assert;

public class HNS11_BlogUpdate {
	
	WebDriver driver = null;
	WebElement element = null;
	String screenShotName = null;
	String testCaseName = "HNS9_BlogDeclineFlow";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Blog blog = new Blog();
	Date systemDate = new Date();
	Admin admin = new Admin();
	Map<String, String> data = new HashMap<String, String>();
	DateConverter dateConverter = new DateConverter();
	WebDriverWait wait;
	UploadFile file = new UploadFile();

	@BeforeClass
	public void beforeClass() throws Exception {
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void updateBlog() throws Exception {
		try {
			TestLog.init(testCaseName, "Verify stylish should be able to Update the blog");
			// Load Test Data
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			// Login as Stylish and visit Blog Tab
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Add date in the Blog title and Create a blog
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
			String blogName = data.get("BlogTitle") + "_" + dateFormat.format(systemDate);
			data.replace("BlogTitle", blogName);
			blog.createBlog(driver, data);

			Thread.sleep(5000);
			user.logout(driver);

			// Login with Admin search for the blog
			admin.login(driver, data.get("AdminId"), data.get("AdminPassword"));
			admin.searchContent(driver, data.get("BlogTitle"));
			admin.updateBlogStatus(driver, "Decline");
			admin.logout(driver);

			// Login with Stylish user
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Search blog in Declined tab
			WebElement blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Declined blog was not found in 'Declined' tab", blogDeclined);

			// Edit a blog
			element = blogDeclined.findElement(By.xpath(Property.get("Profile.declineBlogEdit")));
			element.click();
			element = driver.findElement(By.className(Property.get("Profile.createPostFrame")));
			driver.switchTo().frame(element);

			// Verify Blog details are correct in edit mode
			element = driver.findElement(By.className(Property.get("Profile.blogTitle")));
			String title = element.getText();
			Assert.assertEquals("Title of Blog is not as expected when stylish edit the blog", data.get("BlogTitle"),
					title);
			element = driver.findElement(By.className(Property.get("Profile.blogRemoveImage")));
			Assert.assertEquals("Image of Blog is not shown when stylish edit the blog", element.isDisplayed());
			element = driver.findElement(By.className(Property.get("Profile.blogAlternateText")));
			String alternateText = element.getText();
			Assert.assertEquals("Alternate Text of Blog is not as expected when stylish edit the blog",
					data.get("AlternateText"), alternateText);
			element = driver.findElement(By.xpath(Property.get("Profile.blogEditorFrame")));
			driver.switchTo().frame(element);
			element = driver.findElement(By.tagName("body"));
			String body = element.getText();
			Assert.assertEquals("Body of article is not as expected when stylish edit the blog", data.get("Body"),
					body);
			driver.switchTo().defaultContent();
			element = driver.findElement(By.className(Property.get("Profile.blogTags")));
			String tags = element.getText();
			Assert.assertEquals("Tags of Blog is not as expected when stylish edit the blog", data.get("Tags"), tags);

			Map<String, String> updatedData = excel.readbyrow(testCaseName, 2);

			element = driver.findElement(By.xpath(Property.get("Profile.blogTitle")));
			element.clear();
			element.sendKeys(updatedData.get("BlogTitle"));

			if (!updatedData.get("Profile.blogUploadImage").equalsIgnoreCase("NA")) {
				element = driver.findElement(By.xpath(Property.get("Profile.blogUploadImage")));
				element.click();
				String imagePath = System.getProperty("user.dir") + "/src/com/hns/Testdata/Images/"
						+ updatedData.get("BlogImage");
				file.upload(imagePath);

				element = driver.findElement(By.xpath(Property.get("Profile.blogUpload")));
				element.click();

				wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions.elementToBeClickable(
						driver.findElement(By.xpath(Property.get("Profile.blogRemoveImage")))));
			}

			element = driver.findElement(By.xpath(Property.get("Profile.blogAlternateText")));
			element.clear();
			element.sendKeys(updatedData.get("AlternateText"));

			element = driver.findElement(By.xpath(Property.get("Profile.blogEditorFrame")));
			driver.switchTo().frame(element);
			element = driver.findElement(By.tagName("body"));
			element.clear();
			element.sendKeys(updatedData.get("Body"));
			driver.switchTo().defaultContent();

			element = driver.findElement(By.className(Property.get("Profile.createPostFrame")));
			driver.switchTo().frame(element);

			element = driver.findElement(By.xpath(Property.get("Profile.blogTags")));
			element.clear();
			element.sendKeys(updatedData.get("Tags"));

			element = driver.findElement(By.xpath(Property.get("Profile.blogSubmit")));
			Browser.click(driver, element);

			element = driver.findElement(By.xpath(Property.get("Profile.blogConfirmation")));
			wait.until(ExpectedConditions.textToBePresentInElement(element, "Confirmation"));

			driver.switchTo().defaultContent();
			element = driver.findElement(By.xpath(Property.get("Profile.blogCloseFrame")));
			Browser.click(driver, element);

		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);

			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@AfterClass
	public void afterClass() {
	}

}

package com.hns.Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Admin;
import com.hns.Genric.Blog;
import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.DateConverter;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS9_BlogDeclineFlow {
	
	WebDriver driver = null;
	WebElement element = null;
	String screenshotName = null;
	String testCaseName = "HNS9_BlogDeclineFlow";
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Blog blog = new Blog();
	Date systemDate = new Date();
	Map<String, String> data = new HashMap<String, String>();
	DateConverter dateConverter = new DateConverter();
	WebDriverWait wait;
	Admin admin = new Admin();
	int stepno = 0;
	
	@BeforeClass
	public void before() throws Exception {
		System.out.println(testCaseName + "Starting execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void TestBlogDeclineFlow() throws Exception {

		try {
			TestLog.init(testCaseName, "Verify the flow of blog in various status of blog");
			// Load Test Data
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			// Login as Stylish and visit Blog Tab
			TestLog.step(++stepno, "Login as Stylish and visit Blog Tab");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Add date in the Blog title and create a blog
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
			String blogName = data.get("BlogTitle") + "_" + dateFormat.format(systemDate);
			data.replace("BlogTitle", blogName);
			TestLog.step(++stepno, "Create a Blog");
			blog.createBlog(driver, data);

			Thread.sleep(5000);
			TestLog.step(++stepno, "Logout from Stylish account");
			user.logout(driver);

			// Login with Admin search for the blog and upadte the status to decline
			TestLog.step(++stepno, "Login with Admin search for the blog and update the status to Decline");
			admin.login(driver, data.get("AdminId"), data.get("AdminPassword"));
			admin.searchContent(driver, data.get("BlogTitle"));
			admin.updateBlogStatus(driver, "Decline");
			TestLog.step(++stepno, "Logout from Admin account");
			admin.logout(driver);

			// Login with Stylish user
			TestLog.step(++stepno, "Login with Stylish user");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Search blog in Pending tab
			TestLog.step(++stepno, "Verify blog should not appear in Pending tab");
			WebElement blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNull("Declined blog appears in 'Pending' tab", blogPending);

			// Search blog in Live tab
			TestLog.step(++stepno, "Verify blog should not appear in Live tab");
			WebElement blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNull("Declined blog appears in 'Live' tab", blogLive);

			// Search blog in Declined tab
			TestLog.step(++stepno, "Verify blog should appear in Declined tab");
			WebElement blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Declined blog does not appear in 'Declined' tab", blogDeclined);

			TestLog.step(++stepno, "Verify blog date in Declined tab");
			element = blogDeclined.findElement(By.xpath(Property.get("Profile.declinedBlogDate")));
			String actualBlogDate = element.getText();
			String expectedDate = dateConverter.getCurrentDateInBlogFormat();
			Assert.assertEquals("Blog date in declined list is shown incorrect", expectedDate, actualBlogDate);

			// Edit and Submit a blog for Approval
			TestLog.step(++stepno, "Edit and Submit a blog for Approval");
			element = blogDeclined.findElement(By.xpath(Property.get("Profile.declineBlogEdit")));
			element.click();
			element = driver.findElement(By.className(Property.get("Profile.createPostFrame")));
			driver.switchTo().frame(element);
			element = driver.findElement(By.xpath(Property.get("Profile.blogSubmit")));
			Browser.click(driver, element);
			element = driver.findElement(By.xpath(Property.get("Profile.blogConfirmation")));
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.textToBePresentInElement(element, "Confirmation"));
			driver.switchTo().defaultContent();
			element = driver.findElement(By.xpath(Property.get("Profile.blogCloseFrame")));
			Browser.click(driver, element);

			// Search blog in Live tab
			TestLog.step(++stepno, "Verify Blog should not appear in Live tab");
			blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNull("Submitted declined blog appears in 'Live' tab", blogLive);

			// Search blog in Declined tab
			TestLog.step(++stepno, "Verify Blog should not appear in Declined tab");
			blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNull("Submitted declined blog appears in 'Declined' tab", blogDeclined);

			// Search blog in Pending tab
			TestLog.step(++stepno, "Verify Blog should appear in Pending tab");
			blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Submitted blog does not appear in 'Pending' tab", blogPending);
			TestLog.step(++stepno, "Logout from Stylish account");
			user.logout(driver);

			// Login with admin and search a blog
			TestLog.step(++stepno, "Login with Admin credentials adn update status of blog to Approved");
			admin.login(driver, data.get("AdminId"), data.get("AdminPassword"));
			admin.searchContent(driver, data.get("BlogTitle"));
			admin.updateBlogStatus(driver, "Approve");
			TestLog.step(++stepno, "Logout from Admin account");
			admin.logout(driver);

			// Login as Stylish and visit Blog Tab
			TestLog.step(++stepno, "Login as Stylish and visit Blog Tab");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Search blog in Declined tab
			TestLog.step(++stepno, "Verify blog should not appear in Declined tab");
			blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNull("Approved blog appears in 'Declined' tab", blogDeclined);
			// Search blog in Pending tab
			TestLog.step(++stepno, "Verify blog should not appear in Pending tab");
			blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNull("Approved blog appears in 'Pending' tab", blogPending);
			// Search blog in Live tab
			TestLog.step(++stepno, "Verify blog should appear in Live tab");
			blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Approved blog does not appears in 'Live' tab", blogLive);

			// Verify date of Blog in live tab
			TestLog.step(++stepno, "Verify date of Blog in live tab");
			element = blogLive.findElement(By.xpath(Property.get("Profile.liveBlogDate")));
			String actualDate = element.getText();
			expectedDate = dateConverter.getCurrentDateInBlogFormat();
			Assert.assertEquals("Date of blog in 'live' is not as expected", expectedDate, actualDate);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Falied execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@AfterClass
	public void afterClass() {
		System.out.println(testCaseName + "Shutting down execution...");
		driver.quit();
	}

}

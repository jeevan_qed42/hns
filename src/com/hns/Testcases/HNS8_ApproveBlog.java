package com.hns.Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Admin;
import com.hns.Genric.Blog;
import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.DateConverter;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS8_ApproveBlog {

	WebDriver driver = null;
	WebElement element = null;
	String screenShotName = null;
	String testCaseName = "HNS8_ApproveBlog";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Blog blog = new Blog();
	Date systemDate = new Date();
	Map<String, String> data = new HashMap<String, String>();
	DateConverter dateConverter = new DateConverter();
	WebDriverWait wait;
	Admin admin = new Admin();
	int stepno = 0;

	@BeforeClass
	public void before() throws Exception {
		System.out.println(testCaseName + "Starting execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void approveBlog() throws Exception {

		try {
			TestLog.init(testCaseName, "Verify admin can approve blogs and blog appear in Live tab");
			// Load Test Data
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			// Login as Stylish and visit Blog Tab
			TestLog.step(++stepno, "Login as Stylish and visit Blog Tab");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Add date in the Blog title
			TestLog.step(++stepno, "Add date in the Blog title");
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
			String blogName = data.get("BlogTitle") + "_" + dateFormat.format(systemDate);
			data.replace("BlogTitle", blogName);
			blog.createBlog(driver, data);

			Thread.sleep(5000);
			TestLog.step(++stepno, "Logout from stylish account");
			user.logout(driver);

			// Login with Admin search for the blog and update the status to  Approved
			TestLog.step(++stepno, "Login with Admin search for the blog and update the status to  Approved");
			admin.login(driver, data.get("AdminId"), data.get("AdminPassword"));
			admin.searchContent(driver, data.get("BlogTitle"));
			admin.updateBlogStatus(driver, "Approve");
			TestLog.step(++stepno, "Logout from admin account");
			admin.logout(driver);

			// Login with Stylish user
			TestLog.step(++stepno, "Login with Stylish user");
			user.login(driver, data.get("StylishEmailID"), data.get("StylishPassword"));
			element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("HomePage.menuViewProfile")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("Profile.blogTab")));
			element.click();

			// Search blog in Declined tab
			TestLog.step(++stepno, "Verify blog should not appear in Declined tab");
			WebElement blogDeclined = blog.searchInDeclined(driver, data.get("BlogTitle"));
			Assert.assertNull("Approved blog appears in 'Declined' tab", blogDeclined);
			// Search blog in Pending tab
			TestLog.step(++stepno, "Verify blog should not appear in Pending tab");
			WebElement blogPending = blog.searchInPending(driver, data.get("BlogTitle"));
			Assert.assertNull("Approved blog appears in 'Pending' tab", blogPending);
			// Search blog in Live tab
			TestLog.step(++stepno, "Verify blog should appear in Live tab");
			WebElement blogLive = blog.searchInLive(driver, data.get("BlogTitle"));
			Assert.assertNotNull("Approved blog does not appears in 'Live' tab", blogLive);

			// Verify date of Blog in live tab
			TestLog.step(++stepno, "Verify blog date in Live tab");
			element = blogLive.findElement(By.xpath(Property.get("Profile.liveBlogDate")));
			String actualDate = element.getText();
			String expectedDate = dateConverter.getCurrentDateInBlogFormat();
			Assert.assertEquals("Date of blog in 'live' is not as expected", expectedDate, actualDate);

			// Verify deatisl of blog by accessing the blog
			TestLog.step(++stepno, "Verify details of blog by accessing the blog");
			element = blogLive.findElement(By.xpath(Property.get("Profile.approvedBlogTitle")));
			element.click();
			blog.verifyBlogDetails(driver, data);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenShotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenShotName);
			throw error;
		}

	}

	@AfterClass
	public void after() throws Exception {
		System.out.println(testCaseName + "Shutting down execution...");
		driver.quit();
	}
}

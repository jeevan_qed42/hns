package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS3_SignUpFacebook {

	WebDriver driver = null;
	WebElement element = null;
	String testCaseName = "HNS3_SignUpFacebook";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	Map<String, String> data = new HashMap<String, String>();
	User user = new User();
	int stepno = 0;

	@BeforeMethod
	public void before() throws Exception {
		System.out.println(testCaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void signUpUsingFacebook() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 1", "Verify user should be able to signup using facebook");
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();
			TestLog.step(++stepno, "Enter details and signup using facebook");
			user.signUpUsingFacebook(driver, data);
			TestLog.step(++stepno, "Verify details after signup using facebook");
			user.signUpVerifyDetails(driver, data);
			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@Test(dependsOnMethods = { "signUpUsingFacebook" })
	public void signUpUsingExistingFacebookId() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 2", "Verify user should get logged in if user tries to sign up with existing facebook ID");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();
			TestLog.step(++stepno, "Click on singUp using Facebook button");
			element = driver.findElement(By.xpath(Property.get("Register.singUpFacebook")));
			element.click();
			
			element = driver.findElement(By.xpath(Property.get("Register.facebookEmailId")));
			element.clear();
			element.sendKeys(data.get("FacebookID"));
			element = driver.findElement(By.xpath(Property.get("Register.facebookPassword")));
			element.clear();
			element.sendKeys(data.get("FacebookPassword"));
			element = driver.findElement(By.xpath(Property.get("Register.facebookSubmit")));
			element.click();

			if (driver.getCurrentUrl().contains("https://www.facebook.com")) {
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.facebookOkay")))));

				element = driver.findElement(By.xpath(Property.get("Register.facebookOkay")));
				element.click();

			}

			TestLog.step(++stepno, "Verify user should get login, if user tries to sign up using existing facebook Id");
			element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());

			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@Test(dependsOnMethods = { "signUpUsingFacebook" })
	public void signInUsingFacebook() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 3", "Verify user should be able to sign in with facebook");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();
			TestLog.step(++stepno, "Click on Login button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeLogin")));
			element.click();
			TestLog.step(++stepno, "Click on Sign In using Facebook button");
			element = driver.findElement(By.xpath(Property.get("LoginPage.signInUsingFacebook")));
			element.click();
			
			TestLog.step(++stepno, "Enter Facebook details");
			element = driver.findElement(By.xpath(Property.get("Register.facebookEmailId")));
			element.clear();
			element.sendKeys(data.get("FacebookID"));
			element = driver.findElement(By.xpath(Property.get("Register.facebookPassword")));
			element.clear();
			element.sendKeys(data.get("FacebookPassword"));
			element = driver.findElement(By.xpath(Property.get("Register.facebookSubmit")));
			element.click();

			if (driver.getCurrentUrl().contains("https://www.facebook.com")) {
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.facebookOkay")))));

				element = driver.findElement(By.xpath(Property.get("Register.facebookOkay")));
				element.click();

			}

			TestLog.step(++stepno, "Verify user should be logged in");
			element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
			Assert.assertTrue("User failed to login when trying to login with facebook ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
			Assert.assertTrue("User failed to login when trying to login with facebook ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
			Assert.assertTrue("User failed to login when trying to login with facebook ID", element.isDisplayed());

			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}
	
	
	@AfterMethod
	public void after() throws Exception {
		System.out.println(testCaseName + ": Shutting down execution...");
		driver.quit();
	}
}

package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS4_SignUpTwitter {

	WebDriver driver = null;
	WebElement element = null;
	String testCaseName = "HNS4_SignUpTwitter";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	Map<String, String> data = new HashMap<String, String>();
	User user = new User();
	int stepno = 0;

	@BeforeMethod
	public void before() throws Exception {
		System.out.println(testCaseName + ": Starting execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
	}

	@Test
	public void SignUpUsingTwitter() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 1", "Verify user should be able to sign up using Twitter");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();
			TestLog.step(++stepno, "Enter user data and sign up using Twitter");
			user.signUpUsingTwitter(driver, data);
			TestLog.step(++stepno, "Verify user data after sign up using Twitter");
			user.signUpVerifyDetails(driver, data);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@Test(dependsOnMethods = { "SignUpUsingTwitter" })
	public void signUpUsingExistingTwitterId() throws Exception {
		try {
			TestLog.init(testCaseName + " - Scenario 2",
					"Verify user should get logged in, when user tries to sign up using exiting Twitter ID");
			stepno = 0;
			TestLog.step(++stepno, "Fetch data from excel");
			data = excel.readbyrow(testCaseName, 1);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();
			TestLog.step(++stepno, "Click on Get Started button");
			element = driver.findElement(By.xpath(Property.get("HomePage.homeGetStarted")));
			element.click();
			TestLog.step(++stepno, "Click on SignUp using Twitter");
			element = driver.findElement(By.xpath(Property.get("Register.singUpTwitter")));
			element.click();

			element = driver.findElement(By.xpath(Property.get("Register.twitterUsername")));
			element.clear();
			element.sendKeys(data.get("TwitterUsername"));

			element = driver.findElement(By.xpath(Property.get("Register.twitterPassword")));
			element.clear();
			element.sendKeys(data.get("TwitterPassword"));

			element = driver.findElement(By.xpath(Property.get("Register.twitterLogin")));
			element.click();

			if (driver.getCurrentUrl().contains("https://api.twitter.com/oauth/")) {
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions
						.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.twitterLogin")))));

				element = driver.findElement(By.xpath(Property.get("Register.twitterLogin")));
				element.click();
			}

			TestLog.step(++stepno, "Verify user should get logged in");
			element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
			Assert.assertTrue("User failed to login when trying to register with existing ID", element.isDisplayed());
			TestLog.step(++stepno, "Logout from account");
			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testCaseName);
			System.out.println("Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@AfterMethod
	public void after() throws Exception {
		System.out.println(testCaseName + ": Shutting down execution...");
		driver.quit();
	}
}

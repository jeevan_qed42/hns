package com.hns.Testcases;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hns.Genric.Browser;
import com.hns.Genric.TestLog;
import com.hns.Genric.User;
import com.hns.Utilities.ReadExcel;
import com.hns.Utilities.Property;
import com.hns.Utilities.Screenshot;

import junit.framework.Assert;

public class HNS1_SignInMail {
	
	WebDriver driver = null;
	WebElement element = null;
	String testcaseName = "HNS1_SignInMail";
	String screenshotName = null;
	ReadExcel excel = new ReadExcel();
	User user = new User();
	Map<String, String> data = new HashMap<String, String>();
	int stepno = 0;

	@BeforeClass
	public void before() throws Exception {
		System.out.println(testcaseName + ": Started execution...");
		driver = Browser.launchBrowser(Property.get("GlobalConfig.url"));
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(By.xpath(Property.get("HomePage.homeLogin"))).click();
	}

	@Test(priority = 1)
	public void invalidUserName() throws Exception {
		try {
			TestLog.init(testcaseName + " - Scenario 1", "Verify user should not be able to login with invalid username");
			stepno = 0;
			data = excel.byvalue(testcaseName, "Scenario1");
			TestLog.step(++stepno, "Enter username");
			element = driver.findElement(By.id(Property.get("LoginPage.emailUsername")));
			element.clear();
			element.sendKeys(data.get("UserName"));
			TestLog.step(++stepno, "Enter Password");
			element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
			element.clear();
			element.sendKeys(data.get("Password"));
			TestLog.step(++stepno, "Click on Submit button");
			element = driver.findElement(By.xpath(Property.get("LoginPage.submitLogin")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("LoginPage.invalidCredential")));
			TestLog.step(++stepno, "Verify error message");
			String errorMessage = element.getText();
			Assert.assertEquals("Sorry, unrecognized username or password. Have you forgotten your password?", errorMessage);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testcaseName);
			System.out.println("Scenario1: Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}

	}

	@Test(priority = 2)
	public void invalidPassword() throws Exception {

		try {
			TestLog.init(testcaseName + " - Scenario 2", "Verify user should not be able to login with invalid password");
			stepno = 0;
			data = excel.byvalue(testcaseName, "Scenario2");
			TestLog.step(++stepno, "Enter username");
			element = driver.findElement(By.id(Property.get("LoginPage.emailUsername")));
			element.clear();
			element.sendKeys(data.get("UserName"));
			TestLog.step(++stepno, "Enter invalid Password");
			element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
			element.clear();
			element.sendKeys(data.get("Password"));
			TestLog.step(++stepno, "Click on Submit button");
			element = driver.findElement(By.xpath(Property.get("LoginPage.submitLogin")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("LoginPage.invalidCredential")));
			TestLog.step(++stepno, "Verify error message");
			String errorMessage = element.getText();
			Assert.assertEquals("Sorry, unrecognized username or password. Have you forgotten your password?", errorMessage);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testcaseName);
			System.out.println("Scenario2: Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@Test(priority = 3)
	public void emptyFields() throws Exception {
		try {
			TestLog.init(testcaseName + " - Scenario 3", "Verify user should not be able to login with empty username and password");
			stepno = 0;
			TestLog.step(++stepno, "Enter blank username");
			element = driver.findElement(By.id(Property.get("LoginPage.emailUsername")));
			element.clear();
			TestLog.step(++stepno, "Enter blank pawword");
			element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
			element.clear();
			TestLog.step(++stepno, "Click on Submit button");
			element = driver.findElement(By.xpath(Property.get("LoginPage.submitLogin")));
			element.click();
			element = driver.findElement(By.xpath(Property.get("LoginPage.invalidCredential")));
			TestLog.step(++stepno, "Verify error message");
			String errorMessage = element.getText();
			Assert.assertEquals("Email or username field is required. Password field is required.", errorMessage);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testcaseName);
			System.out.println("Scenario3: Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@Test(priority = 4)
	public void validLogin() throws Exception {
		try {
			TestLog.init(testcaseName + " - Scenario 4", "Verify user should be able to login with valid username and password");
			stepno = 0;
			data = excel.byvalue(testcaseName, "Scenario4");
			TestLog.step(++stepno, "Enter username");
			element = driver.findElement(By.id(Property.get("LoginPage.emailUsername")));
			element.clear();
			element.sendKeys(data.get("UserName"));
			TestLog.step(++stepno, "Enter username");
			element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
			element.clear();
			element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
			element.sendKeys(data.get("Password"));
			TestLog.step(++stepno, "Click on Login page");
			element = driver.findElement(By.xpath(Property.get("LoginPage.submitLogin")));
			element.click();
			TestLog.step(++stepno, "Verify user is logged in");
			element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
			Assert.assertTrue("User failed to login", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
			Assert.assertTrue("User failed to login", element.isDisplayed());
			element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
			Assert.assertTrue("User failed to login", element.isDisplayed());
			TestLog.step(++stepno, "Logout the user");
			user.logout(driver);
			TestLog.pass();
		} catch (Exception | AssertionError error) {
			if (driver != null)
				screenshotName = Screenshot.capture(driver, testcaseName);
			System.out.println("Scenario4: Failed execution...");
			TestLog.fail(screenshotName);
			throw error;
		}
	}

	@AfterClass
	public void after() throws Exception {
		System.out.println(testcaseName + ": Shutting down execution...");
		driver.quit();
	}

}

package com.hns.Genric;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hns.Utilities.Property;

public class Admin {
	WebElement element;
	
	/*
	 * To search the content through admin login
	 */
	public void searchContent(WebDriver driver, String contentTitle) throws Exception{
		
		element = driver.findElement(By.xpath(Property.get("Admin.menuContent")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Admin.contentSearchBox")));
		element.sendKeys(contentTitle);
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Property.get("Admin.noLoader"))));

	}
	
	/*
	 * To update the status variable of the blog
	 */
	public void updateBlogStatus(WebDriver driver, String status) throws Exception {
		
		element = driver.findElement(By.linkText(Property.get("Admin.contentEdit")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Admin.contentStatusDropDown")));
		Select blogStatus = new Select(element);
		blogStatus.selectByVisibleText(status);
		element = driver.findElement(By.xpath(Property.get("Admin.contentSave")));
		element.click();
	}

	/*
	 * To login as a admin
	 */
	public void login(WebDriver driver, String UserName, String Password) throws Exception {	
		User user = new User();
		user.login(driver, UserName, Password);
	}
	
	/*
	 * To logout from admin account
	 */
	public void logout(WebDriver driver) throws Exception {
		element = driver.findElement(By.xpath(Property.get("Admin.adminLogout")));
		element.click();
	}
}

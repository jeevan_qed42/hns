package com.hns.Genric;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hns.Utilities.DateConverter;
import com.hns.Utilities.Property;
import com.hns.Utilities.UploadFile;


import junit.framework.Assert;

public class Blog {

	WebDriver driver = null;
	WebElement element = null;
	UploadFile file = new UploadFile();
	WebDriverWait wait;
	DateConverter dateConverter = new DateConverter();

	/*
	 * Function to create a new blog
	 */
	public void createBlog(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Profile.createDraft")));
		element.click();

		element = driver.findElement(By.className(Property.get("Profile.createPostFrame")));
		driver.switchTo().frame(element);

		element = driver.findElement(By.xpath(Property.get("Profile.blogTitle")));
		element.sendKeys(data.get("BlogTitle"));

		if (!data.get("BlogImage").equalsIgnoreCase("NA")) {
			element = driver.findElement(By.xpath(Property.get("Profile.blogUploadImage")));
			element.click();
			String imagePath = System.getProperty("user.dir") + "/src/com/hns/Testdata/Images/" + data.get("BlogImage");
			file.upload(imagePath);
			element = driver.findElement(By.xpath(Property.get("Profile.blogUpload")));
			element.click();
			wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(Property.get("Profile.blogRemoveImage")))));
		}

		element = driver.findElement(By.xpath(Property.get("Profile.blogAlternateText")));
		element.sendKeys(data.get("AlternateText"));

		element = driver.findElement(By.xpath(Property.get("Profile.blogEditorFrame")));
		driver.switchTo().frame(element);
		element = driver.findElement(By.tagName("body"));
		element.clear();
		element.sendKeys(data.get("Body"));
		driver.switchTo().defaultContent();

		element = driver.findElement(By.className(Property.get("Profile.createPostFrame")));
		driver.switchTo().frame(element);

		element = driver.findElement(By.xpath(Property.get("Profile.blogTags")));
		element.sendKeys(data.get("Tags"));

		element = driver.findElement(By.xpath(Property.get("Profile.blogCategory")));
		element.click();
		Thread.sleep(2000);
		element = driver.findElement(By.xpath("//li[contains(text(),'" + data.get("Category") + "')]"));
		element.click();
		Thread.sleep(2000);
		element = driver.findElement(By.xpath(Property.get("Profile.blogSubmit")));
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().build().perform();

		element = driver.findElement(By.xpath(Property.get("Profile.blogCreationMessage")));
		Assert.assertEquals("Wrong message shown after creation of blog",
				"Blog Post " + data.get("BlogTitle") + " has been created.", element.getText());

		element = driver.findElement(By.xpath(Property.get("Profile.blogConfirmation")));
		wait.until(ExpectedConditions.textToBePresentInElement(element, "Confirmation"));

		driver.switchTo().defaultContent();
		element = driver.findElement(By.xpath(Property.get("Profile.blogCloseFrame")));
		Browser.click(driver, element);

	}

	/*
	 * Search a blog in Live tab and return the blog
	 */
	public WebElement searchInLive(WebDriver driver, String blogTitle) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Profile.blogsLive")));
		try {
			element.click();
		} catch (WebDriverException error) {
			Browser.click(driver, element);
		}
		WebElement requiredBlog = null;
		List<WebElement> blogList = driver.findElements(By.xpath(Property.get("Profile.approvedBlogList")));
		if (!blogList.isEmpty()) {
			for (WebElement blog : blogList) {
				String title = blog.findElement(By.xpath(Property.get("Profile.approvedBlogTitle"))).getText();
				if (title.equals(blogTitle)) {
					requiredBlog = blog;
					break;
				}
			}
		}
		return requiredBlog;
	}

	/*
	 * Search a blog in Pending tab and return the blog
	 */
	public WebElement searchInPending(WebDriver driver, String blogTitle) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Profile.blogsPending")));
		try {
			element.click();
		} catch (WebDriverException error) {
			Browser.click(driver, element);
		}
		WebElement requiredBlog = null;
		List<WebElement> blogList = driver.findElements(By.xpath(Property.get("Profile.pendingBlogList")));
		if (!blogList.isEmpty()) {
			for (WebElement blog : blogList) {
				String title = blog.findElement(By.xpath(Property.get("Profile.pendingBlogTitle"))).getText();
				if (title.equals(blogTitle)) {
					requiredBlog = blog;
					break;
				}
			}
		}
		return requiredBlog;
	}

	/*
	 * Search a blog in Declined tab and return the blog
	 */
	public WebElement searchInDeclined(WebDriver driver, String blogTitle) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Profile.blogsDeclined")));
		try {
			element.click();
		} catch (WebDriverException error) {
			Browser.click(driver, element);
		}

		WebElement requiredBlog = null;
		List<WebElement> blogList = driver.findElements(By.xpath(Property.get("Profile.declinedBlogList")));
		if (!blogList.isEmpty()) {
			for (WebElement blog : blogList) {
				String title = blog.findElement(By.xpath(Property.get("Profile.declinedBlogTitle"))).getText();
				if (title.equals(blogTitle)) {
					requiredBlog = blog;
					break;
				}
			}
		}
		return requiredBlog;

	}

	public void deleteBlog() throws Exception {
		element = driver.findElement(By.xpath(Property.get("Profile.declinedBlogDelete")));
		Browser.click(driver, element);

		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions
				.visibilityOf((driver.findElement(By.xpath(Property.get("Profile.declinedBlogCancel"))))));
		element = driver.findElement(By.xpath(Property.get("Profile.declinedBlogConfirmDelete")));
		Browser.click(driver, element);
	}

	/**
	 * This function will verify the teaser of blogs in The Parlour page
	 * 
	 * @throws Exception
	 */
	public void verifyBlogTeaserInParlour(WebElement foundBlog, String expectedBlogColour, String userName)
			throws Exception {

		element = foundBlog.findElement(By.xpath(Property.get("ParlourPage.blogUsername")));
		String username = element.getText();
		Assert.assertEquals("Username in Teaser is inccorect in Parlours page", "@" + userName, username);

		String blogDate = foundBlog.findElement(By.xpath(Property.get("ParlourPage.blogDate"))).getText();
		Assert.assertEquals("Blog Date in Teaser is inccorect in Parlours page",
				dateConverter.getCurrentDateInBlogFormat(), blogDate);

		String blogColour = foundBlog.findElement(By.xpath(Property.get("ParlourPage.blogTeaser")))
				.getAttribute("data-bg-color");
		Assert.assertEquals("Blog Colour in Teaser is inccorect in Parlours page", expectedBlogColour, blogColour);

	}

	/**
	 * This function will search of blog in the parlour page
	 * 
	 * @throws Exception
	 */
	public WebElement searchBlog(WebDriver driver, String blogName) throws Exception {
		WebElement requiredBlog = null;
		int pageNumber = 1;

		for (;;) {
			try {
				driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				driver.findElement(By.xpath(Property.get("ParlourPage.blogView")));
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			} catch (Exception e) {
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				break;
			}
			List<WebElement> blogs = driver.findElements(By.xpath(Property.get("ParlourPage.listOfBlogs")));

			for (WebElement blog : blogs) {

				element = blog.findElement(By.xpath(Property.get("ParlourPage.blogTitle")));
				if (blogName.equalsIgnoreCase(element.getText())) {
					requiredBlog = element;
					break;
				}
			}

			if (requiredBlog == null) {
				String url = driver.getCurrentUrl();
				if (url.contains("?page")) {
					url = url.substring(0, url.indexOf("=")) + "=" + pageNumber;
					driver.get(url);
					pageNumber++;
				} else {
					url = url + "?page=" + pageNumber;
					driver.get(url);
					pageNumber++;
				}
			} else {
				break;
			}
		}

		return requiredBlog;
	}

	/*
	 * To verify the details displayed in the main blog
	 */
	public void verifyBlogDetails(WebDriver driver, Map<String, String> data) throws Exception {

		String expectedDate = dateConverter.getCurrentDateInBlogFormat();
		element = driver.findElement(By.xpath(Property.get("Profile.mainBlogTitle")));
		String actualTitle = element.getText();
		Assert.assertEquals("Title of Blog on top is shown incorrect on a main blog page", data.get("BlogTitle"),
				actualTitle);

		element = driver.findElement(By.xpath(Property.get("Profile.topUsername")));
		String actualUsername = element.getText();
		Assert.assertEquals("Username of Blog on top is shown incorrect on a main blog page", data.get("StylishUserId"),
				actualUsername);

		element = driver.findElement(By.xpath(Property.get("Profile.topCreatedDate")));
		String actualBlogDate = element.getText();
		Assert.assertEquals("Blog date of Blog on top is shown incorrect on a main blog page", "on " + expectedDate,
				actualBlogDate);

		element = driver.findElement(By.xpath(Property.get("Profile.bottomUsername")));
		String actualUsernameBottom = element.getText();
		Assert.assertEquals("On the top of the page Username is shown incorrect", data.get("StylishUserId"),
				actualUsernameBottom);

		element = driver.findElement(By.xpath(Property.get("Profile.mainBlogBannerImage")));
		String alternateText = element.getAttribute("alt");
		Assert.assertEquals("Alternate text of banner image is shown wrong", data.get("AlternateText"), alternateText);

		element = driver.findElement(By.xpath(Property.get("Profile.mainBlogTitle")));
		String actualTitleBottom = element.getText();
		Assert.assertEquals("On the botom of the page Title of Blog is shown incorrect", data.get("BlogTitle"),
				actualTitleBottom);
	}
}

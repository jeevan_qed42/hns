package com.hns.Genric;

import org.testng.Reporter;

public class TestLog {

	/**
	 * Funtion to initalize the test case details
	 * @param testcaseName name of test case under test.
	 * @param description - description of the test case under test.
	 */
	public static void init(String testcaseName, String description) {
		Reporter.log("<br><b>Test Case : </b>" + testcaseName);
		Reporter.log("<br><b>Description : </b>" + description + "<br><br>");
	}

	
	/**
	 * Function to log screenshot if test case fails
	 * @param screenshotName name of image file to be attached as screenshot, pass null to skip the screenshot.
	 */
	public static void fail(String screenshotName) {

		if (screenshotName != null) {
			Reporter.log("<br><a href='screenshots/" + screenshotName
					+ ".png' target='_blank'><img src='screenshots/" + screenshotName
					+ ".png' style='height: 125px; width: 159px; padding: 20px;' target='_blank'></a>");
		} else {
			Reporter.log("<br><br><span style='font-size: 12px;padding: 5px;'>Failed to Capture Screenshot</span>");
		}

		Reporter.log(
				"<br><span style='background-color: #ff4d4d;margin: 20px;display: block;height: 17px;width: 140px;padding: 12px;text-align: center;border-radius: 5px;color: black;font-size: 16px;border: black;margin-top: 1px'> FAILED </span>");

	}

	public static void pass() {
		Reporter.log(
				"<br><br><span style='background-color: #66ff66;margin: 20px;display: block;height: 17px;width: 140px;padding: 12px;text-align: center;border-radius: 5px;color: black;font-size: 16px;border: black;margin-top: 1px'> PASSED </span>");
	}

	/**
	 * 
	 * @param stepno
	 * @param stepDescription
	 */
	public static void step(int stepno, String stepDescription) {
		Reporter.log("<br><b>Step " + stepno + " :</b> " + stepDescription);
	}
}

package com.hns.Genric;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hns.Utilities.Email;
import com.hns.Utilities.Property;

import junit.framework.Assert;

/*
 *  Class to handle the user related activites
 */

public class User {
	WebDriver driver = null;
	WebElement element = null;
	Email email = new Email();

	/*
	 * User Login function
	 */
	public void login(WebDriver driver, String UserName, String Password) throws Exception {

		element = driver.findElement(By.xpath(Property.get("HomePage.homeLogin")));
		element.click();
		element = driver.findElement(By.id(Property.get("LoginPage.emailUsername")));
		element.clear();
		element.sendKeys(UserName);
		element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
		element.clear();
		element = driver.findElement(By.id(Property.get("LoginPage.emailPassword")));
		element.sendKeys(Password);
		element = driver.findElement(By.xpath(Property.get("LoginPage.submitLogin")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("HomePage.userWardrode")));
		Assert.assertTrue("User failed to login", element.isDisplayed());
		element = driver.findElement(By.xpath(Property.get("HomePage.userWishlist")));
		Assert.assertTrue("User failed to login", element.isDisplayed());
		element = driver.findElement(By.xpath(Property.get("HomePage.userBag")));
		Assert.assertTrue("User failed to login", element.isDisplayed());

	}

	/*
	 * User logout function
	 */
	public void logout(WebDriver driver) throws Exception {

		element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("HomePage.menuSignout")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("HomePage.homeLogin")));
		Assert.assertTrue("User failed to logout", element.isDisplayed());

	}

	/*
	 * Function to signup using twitter
	 */
	public void signUpUsingTwitter(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Register.singUpTwitter")));
		element.click();

		element = driver.findElement(By.xpath(Property.get("Register.twitterUsername")));
		element.clear();
		element.sendKeys(data.get("TwitterUsername"));

		element = driver.findElement(By.xpath(Property.get("Register.twitterPassword")));
		element.clear();
		element.sendKeys(data.get("TwitterPassword"));

		element = driver.findElement(By.xpath(Property.get("Register.twitterLogin")));
		element.click();

		if (driver.getCurrentUrl().contains("https://api.twitter.com/oauth/")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.twitterLogin")))));

			element = driver.findElement(By.xpath(Property.get("Register.twitterLogin")));
			element.click();
		}

		element = driver.findElement(By.xpath(Property.get("Register.emailOther")));
		element.sendKeys(data.get("EmailId"));
		element = driver.findElement(By.xpath(Property.get("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(Property.get("Register.signUpLogin")));
		element.click();

		singUpSelectTitle(driver, data);

		element = driver.findElement(By.xpath(Property.get("Register.fullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);

		element = driver.findElement(By.xpath(Property.get("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(Property.get("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(Property.get("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(Property.get("Register.letsGetStarted")));
		element.click();

		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);

	}

	/*
	 * Function to signup using Google
	 */
	public void signUpUsingGoogle(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Register.signUpGoogle")));
		element.click();

		element = driver.findElement(By.xpath(Property.get("Register.gmailUsername")));
		element.clear();
		element.sendKeys(data.get("GoogleUsername"));

		//element = driver.findElement(By.xpath(Property.get("Register.gmailNext")));
		//element.click();

		WebDriverWait wait = new WebDriverWait(driver, 5);
		element = driver.findElement(By.xpath(Property.get("Register.gmailPassword")));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.clear();
		element.sendKeys(data.get("GooglePassword"));

		element = driver.findElement(By.xpath(Property.get("Register.gmailSignIn")));
		element.click();

		if (driver.getCurrentUrl().contains("https://accounts.google.com/o/oauth2/auth?")) {
			wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.gmailAllow")))));

			driver.findElement(By.xpath(Property.get("Register.gmailAllow"))).click();
		}

		element = driver.findElement(By.xpath(Property.get("Register.emailOther")));
		element.sendKeys(data.get("EmailId"));
		element = driver.findElement(By.xpath(Property.get("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(Property.get("Register.signUpLogin")));
		element.click();

		singUpSelectTitle(driver, data);

		element = driver.findElement(By.xpath(Property.get("Register.fullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);

		element = driver.findElement(By.xpath(Property.get("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(Property.get("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(Property.get("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(Property.get("Register.letsGetStarted")));
		element.click();

		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);

	}

	/*
	 * Function to signup using Facebook
	 */
	public void signUpUsingFacebook(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Register.singUpFacebook")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Register.facebookEmailId")));
		element.clear();
		element.sendKeys(data.get("FacebookID"));
		element = driver.findElement(By.xpath(Property.get("Register.facebookPassword")));
		element.clear();
		element.sendKeys(data.get("FacebookPassword"));
		element = driver.findElement(By.xpath(Property.get("Register.facebookSubmit")));
		element.click();

		if (driver.getCurrentUrl().contains("https://www.facebook.com")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath(Property.get("Register.facebookOkay")))));

			element = driver.findElement(By.xpath(Property.get("Register.facebookOkay")));
			element.click();

		}

		element = driver.findElement(By.xpath(Property.get("Register.emailOther")));
		element.sendKeys(data.get("EmailId"));
		element = driver.findElement(By.xpath(Property.get("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("A welcome message with further instructions has been sent to your e-mail address.",
				message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(Property.get("Register.signUpLogin")));
		element.click();

		singUpSelectTitle(driver, data);

		element = driver.findElement(By.xpath(Property.get("Register.fullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals(data.get("FullName"), fullName);

		element = driver.findElement(By.xpath(Property.get("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals(data.get("EmailId"), emailAddress);
		Assert.assertFalse(element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(Property.get("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(Property.get("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(Property.get("Register.letsGetStarted")));
		element.click();

		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);

	}

	/*
	 * Function to select the Title while signup
	 */
	private void singUpSelectTitle(WebDriver driver, Map<String, String> data) throws Exception {
		element = driver.findElement(By.xpath(Property.get("Register.title")));
		element.click();

		switch (data.get("Title")) {
		case "Mr.":
			element = driver.findElement(By.xpath(Property.get("Register.selectMr")));
			element.click();
			break;
		case "Mrs.":
			element = driver.findElement(By.xpath(Property.get("Register.selectMrs")));
			element.click();
			break;
		case "Miss.":
			element = driver.findElement(By.xpath(Property.get("Register.selectMiss")));
			element.click();
			break;
		}
	}

	/**
	 * Function to signup using mail
	 */
	public void signUpUsingMail(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(Property.get("Register.signUpMail")));
		element.click();
		element = driver.findElement(By.className(Property.get("Register.signUpFrame")));
		driver.switchTo().frame(element);

		element = driver.findElement(By.xpath(Property.get("Register.emailPopup")));
		element.sendKeys(data.get("EmailId"));
		element = driver.findElement(By.xpath(Property.get("Register.createAccountPopup")));
		element.click();
		element = driver.findElement(By.xpath(Property.get("Register.statusMessage")));
		String message = element.getText();
		Assert.assertEquals("Incorrect message shown after submit",
				"A welcome message with further instructions has been sent to your e-mail address.", message);
		String signUpLink = email.getSignUpLink(data.get("EmailId"), data.get("EmailPassword"));
		driver.get(signUpLink);
		element = driver.findElement(By.xpath(Property.get("Register.signUpLogin")));
		element.click();

		element = driver.findElement(By.xpath(Property.get("Register.profilePicture")));
		String path = System.getProperty("user.dir") + "/src/com/hns/Testdata/Images/" + data.get("ProfilePicture");
		element.sendKeys(path);

		singUpSelectTitle(driver, data);

		element = driver.findElement(By.xpath(Property.get("Register.fullName")));
		element.clear();
		element.sendKeys(data.get("FullName"));

		element = driver.findElement(By.xpath(Property.get("Register.emailAddress")));
		String emailAddress = element.getAttribute("value");
		Assert.assertEquals("Incorrect mail address shown at Sign Up process", data.get("EmailId"), emailAddress);
		Assert.assertFalse("Email ID field is enabled while sign Up", element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.userName")));
		element.clear();
		element.sendKeys(data.get("UserId"));

		element = driver.findElement(By.xpath(Property.get("Register.password")));
		element.clear();
		element.sendKeys(data.get("Password"));

		element = driver.findElement(By.xpath(Property.get("Register.confirmPassword")));
		element.clear();
		element.sendKeys(data.get("ConfrimPassword"));

		element = driver.findElement(By.xpath(Property.get("Register.letsGetStarted")));
		element.click();

		signUpSelectOptions(driver, data);
		signUpFinalize(driver, data);

	}

	/*
	 * Function to select the steps while sign up
	 */
	private void signUpSelectOptions(WebDriver driver, Map<String, String> data) throws Exception {

		for (int stepNo = 1; stepNo <= 5; stepNo++) {

			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(
					ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='step_" + stepNo + "']"))));

			element = driver.findElement(By.xpath("//div[@id='step_" + stepNo + "']"));
			List<WebElement> totalOptions = driver
					.findElements(By.xpath("//div[@id='edit-step-" + stepNo + "-term']/div[@class='taxonomy_markup']"));
			// System.out.println("Total :" + totalOptions.size() + " Options
			// for Step : " + stepNo);
			if (totalOptions.isEmpty()) {
				throw new Exception("Option not Found for Step: " + stepNo);
			}
			boolean optionFound = false;
			for (WebElement option : totalOptions) {
				// System.out.println("Inside Options");
				element = option.findElement(By.xpath("div[2]/label"));
				String optionText = element.getText();
				// System.out.println("Option : " + optionText);
				// System.out
				// .println("Excel Data of Step" + stepNo + "_Option : " +
				// data.get("Step" + stepNo + "_Option"));
				if (data.get("Step" + stepNo + "_Option").equalsIgnoreCase(optionText)) {
					element = option.findElement(By.xpath("div"));
					element.click();
					optionFound = true;
					break;
				}
			}
			if (!optionFound) {
				throw new Exception(data.get("Step" + stepNo + "_Option") + " : option not found for Step - " + stepNo);
			}
		}
	}

	/**
	 * Function to finalize the sign up process
	 */
	private void signUpFinalize(WebDriver driver, Map<String, String> data) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Property.get("Register.finalFullName")))));

		element = driver.findElement(By.xpath(Property.get("Register.finalFullName")));
		String fullName = element.getAttribute("value");
		Assert.assertEquals("Incorrect Fullname shown while finalizing the sign up", data.get("FullName"), fullName);
		Assert.assertTrue("Full Name is not enable while finalizing the sign up", element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.finalEmailID")));
		String emailId = element.getAttribute("value");
		Assert.assertEquals("Incorrect Email ID shown while finalizing the sign up", data.get("EmailId"), emailId);
		Assert.assertFalse("Email Id is enabled while finalizing the sign up", element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.finalUserName")));
		String UserId = element.getAttribute("value");
		Assert.assertEquals("Incorrect User ID shown while finalizing the sign up", data.get("UserId"), UserId);
		Assert.assertFalse("User ID is enabled while finalizing the sign up", element.isEnabled());

		element = driver.findElement(By.xpath(Property.get("Register.finalBio")));
		element.clear();
		element.sendKeys(data.get("Bio"));

		element = driver.findElement(By.xpath(Property.get("Register.finalizeMyAccount")));
		element.click();
	}

	/*
	 * Function to verify the details of user in profile tab
	 */
	public void signUpVerifyDetails(WebDriver driver, Map<String, String> data) throws Exception {

		element = driver.findElement(By.xpath(Property.get("HomePage.banner")));
		Assert.assertTrue("User landed on wrong page after signup", element.isDisplayed());

		element = driver.findElement(By.xpath(Property.get("HomePage.userProfileMenu")));
		element.click();

		element = driver.findElement(By.xpath(Property.get("HomePage.menuSettings")));
		element.click();

		element = driver.findElement(By.xpath(Property.get("HomePage.settingsProfileTab")));
		element.click();

		element = driver.findElement(By.xpath(Property.get("HomePage.settingsProfileTabFullName")));
		String settingsFullName = element.getAttribute("value");
		Assert.assertEquals("Incorrect profile name shown in profiles", data.get("FullName"), settingsFullName);

		element = driver.findElement(By.xpath(Property.get("HomePage.settingsProfileTabUsername")));
		String settingsUsername = element.getText();
		Assert.assertEquals("Incorrect username shown in profile", data.get("UserId"), settingsUsername);

		element = driver.findElement(By.xpath(Property.get("HomePage.settingsProfileTabBio")));
		String settingsBio = element.getText();
		Assert.assertEquals("Incorrect Bio shown in profile", data.get("Bio"), settingsBio);
	}

}
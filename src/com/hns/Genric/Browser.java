package com.hns.Genric;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.hns.Utilities.Property;

/*
 *  Class to handle the browser activities
 */
public class Browser {

	/*
	 * To launch the browser and return the driver
	 */
	public static WebDriver launchBrowser(String URL) throws Exception {

		WebDriver driver = null;

		String browser = Property.get("GlobalConfig.browser");

		if (browser.equalsIgnoreCase("Firefox")) {
			driver = new FirefoxDriver();
			driver.get(URL);
			driver.manage().window().maximize();

		} else if (browser.equalsIgnoreCase("Chrome")) {
			/*
			 * Toolkit toolkit = Toolkit.getDefaultToolkit(); int Width = (int)
			 * toolkit.getScreenSize().getWidth(); int Height = (int)
			 * toolkit.getScreenSize().getHeight();
			 */
			System.setProperty("webdriver.chrome.driver", "src/com/hns/Utilities/chromedriver");
			driver = new ChromeDriver();
			// driver.manage().window().setSize(new Dimension(Width, Height));
			driver.manage().window().maximize();
			driver.get(URL);

		}
		return driver;
	}

	/*
	 * To click on element using JS, to handle chrome browser issue
	 */
	public static void click(WebDriver driver, WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	/*
	 * To scroll down using JS
	 */
	public static void scrollDown(WebDriver driver) {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("scroll(0, 250);");
		
	}

	public static boolean checkElementPresent(WebDriver driver, String locator, String location) {
		boolean foundElement = false;
		WebDriverWait wait = null;
		switch (locator.toLowerCase()) {
		case "xpath":
			try {
				wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.visibilityOf((WebElement) (By.xpath(location))));

				foundElement = true;
			} catch (Exception error) {

				foundElement = false;
			}
			break;

		case "id":
			try {

				driver.findElement(By.id(location));
				foundElement = true;
			} catch (Exception error) {

				foundElement = false;
			}
			break;

		case "classname":
			try {

				driver.findElement(By.className(location));
				foundElement = true;
			} catch (Exception error) {

				foundElement = false;
			}
			break;

		case "cssselector":
			try {

				driver.findElement(By.cssSelector(location));
				foundElement = true;
			} catch (Exception error) {

				foundElement = false;
			}
			break;
		}

		return foundElement;
	}

}

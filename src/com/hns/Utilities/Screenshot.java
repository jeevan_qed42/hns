package com.hns.Utilities;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {

	/*
	 *  Function to capture the screen shot and return the filename
	 */
	public static String capture(WebDriver driver, String TestCaseName) throws IOException
	{
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmmss");
		Date date = new Date();
		String filename = TestCaseName+"_"+dateFormat.format(date);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("test-output/screenshots/"+filename+".png"));
		
		return filename;
		
	}
}

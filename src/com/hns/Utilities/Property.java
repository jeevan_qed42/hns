package com.hns.Utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/*
 *  Class to handle the functions related to properties files
 */
		
public class Property {
	
	/*
	 *  Function to return the value of key from properties file
	 */
	public static String get(String elements) throws Exception
	{
		String[] properties = elements.split("\\.");
		InputStream input = null;
		Properties prop = new Properties();
		input = new FileInputStream("src/com/hns/ObjectRepo/"+properties[0]+".properties");
		prop.load(input);
		
		if(prop.getProperty(properties[1]) == null) {
			throw new Exception("Key '" + properties[1] + "' not found in Object Repository");
		}
		
		return prop.getProperty(properties[1]);
	}

	
}

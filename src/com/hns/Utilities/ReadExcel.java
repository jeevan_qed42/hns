package com.hns.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
 *  Class to perform the activies realted to excel
 */
public class ReadExcel {

	/*
	 * Function to get the excel row by passing the values of row
	 */
	public Map<String, String> byvalue(String filename, String value) throws Exception {
		
		Map<String, String> exceldata = new HashMap<String, String>();
		FileInputStream file = new FileInputStream(new File("src/com/hns/TestData/" + filename + ".xlsx"));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);

		Iterator<Row> rowIterator = sheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();

			Iterator<Cell> cellIterator = row.cellIterator();

			if (row.getCell(0).toString().toLowerCase().equals(value.toLowerCase())) {
				int cellcount = 0;
				while (cellIterator.hasNext()) {
					Row mainrow = sheet.getRow(0);
					Cell cell = cellIterator.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					exceldata.put(mainrow.getCell(cellcount).toString(), cell.getStringCellValue());
					cellcount++;
				}
			}

		}
		file.close();

		if (exceldata.isEmpty()) {
			throw new Exception("Test Data is empty");
		}

		return exceldata;
	}

	/*
	 * Function to get the excel row by passing the row number
	 */
	public Map<String, String> readbyrow(String Filename, int rownumber) throws Exception {
		
		Map<String, String> exceldata = new HashMap<String, String>();
		FileInputStream file = new FileInputStream(new File("src/com/hns/TestData/" + Filename + ".xlsx"));
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
		Row row = sheet.getRow(rownumber);

		Iterator<Cell> cellIterator = row.cellIterator();

		int cellcount = 0;
		while (cellIterator.hasNext()) {
			Row mainrow = sheet.getRow(0);
			Cell cell = cellIterator.next();
			cell.setCellType(Cell.CELL_TYPE_STRING);
			exceldata.put(mainrow.getCell(cellcount).toString(), cell.getStringCellValue());
			cellcount++;
		}

		file.close();

		if (exceldata.isEmpty()) {
			throw new Exception("Test Data is empty");
		}

		return exceldata;

	}

}

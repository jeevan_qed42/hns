package com.hns.Utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 *  Class to handle the date related function
 */
public class DateConverter {

	Date systemDate = new Date();

	/*
	 * To convert the current date in blog format and return the date Example:
	 * Jaunary 20th, 2016
	 */
	public String getCurrentDateInBlogFormat() {

		DateFormat currentDate = new SimpleDateFormat("dd");
		DateFormat blogDateFormat = new SimpleDateFormat("MMMM dd, yyyy");
		String expectedDate = blogDateFormat.format(systemDate);
		int date = Integer.parseInt(currentDate.format(systemDate));
		switch (date) {

		case 1:
		case 21:
		case 31:

			expectedDate = expectedDate.substring(0, expectedDate.indexOf(" ")) + " " + date + "st"
					+ expectedDate.substring(expectedDate.indexOf(","));
			break;

		case 2:
		case 22:

			expectedDate = expectedDate.substring(0, expectedDate.indexOf(" ")) + " " + date + "nd"
					+ expectedDate.substring(expectedDate.indexOf(","));
			break;

		case 3:
		case 23:

			expectedDate = expectedDate.substring(0, expectedDate.indexOf(" ")) + " " + date + "rd"
					+ expectedDate.substring(expectedDate.indexOf(","));
			break;

		default:

			expectedDate = expectedDate.substring(0, expectedDate.indexOf(" ")) + " " + date + "th"
					+ expectedDate.substring(expectedDate.indexOf(","));
			break;

		}

		return expectedDate;

	}
}
